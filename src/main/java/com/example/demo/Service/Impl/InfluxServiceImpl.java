package com.example.demo.Service.Impl;

import com.example.demo.Model.imumeasurement.ImuMeasurementPoint;
import com.example.demo.Model.session.SessionResponse;
import com.example.demo.Service.InfluxService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class InfluxServiceImpl implements InfluxService {

    @Value("${spring.influx.user}")
    private String influxUser;
    @Value("${spring.influx.password}")
    private String influxPassword;
    @Value("${spring.influx.url}")
    private String influxUrl;

    @Override
    public void savePoints(BatchPoints points) {
        InfluxDB influxDb = InfluxDBFactory.connect(influxUrl, influxUser, influxPassword);
        influxDb.setDatabase("imu");
        influxDb.write(points);
    }

    @Override
    public List<SessionResponse> getAllSessions() {
        InfluxDBResultMapper influxDBResultMapper = new InfluxDBResultMapper();

        List<ImuMeasurementPoint> points = influxDBResultMapper.toPOJO(
            this.influxQuery("SELECT DISTINCT(sessionId) as sessionId FROM measureImu", "imu"),
            ImuMeasurementPoint.class);

        return points.stream().map(point -> new SessionResponse(point.getSessionId())).collect(Collectors.toList());
    }

    @Override
    public SessionResponse getCurrentSession() {
        InfluxDBResultMapper influxDBResultMapper = new InfluxDBResultMapper();

        List<ImuMeasurementPoint> points = influxDBResultMapper.toPOJO(
            this.influxQuery("SELECT * FROM measureImu GROUP BY * ORDER BY DESC LIMIT 1", "imu"),
            ImuMeasurementPoint.class);

        return new SessionResponse(points.get(0).getSessionId());
    }

    @Override
    public List<ImuMeasurementPoint> getPointsInLastMilliseconds(Long milliseconds) {
        InfluxDBResultMapper influxDBResultMapper = new InfluxDBResultMapper();

        return influxDBResultMapper.toPOJO(
            this.influxQuery("SELECT * FROM measureImu WHERE time > now() - " + milliseconds + "ms", "imu"),
            ImuMeasurementPoint.class);
    }

    @Override
    public List<ImuMeasurementPoint> getPointsInLastSeconds(Long seconds) {
        InfluxDBResultMapper influxDBResultMapper = new InfluxDBResultMapper();
        return influxDBResultMapper.toPOJO(
            this.influxQuery("SELECT * FROM measureImu WHERE time > now() - " + seconds + "s", "imu"),
                ImuMeasurementPoint.class);
    }

    @Override
    public List<ImuMeasurementPoint> getAllPoints() {
        InfluxDBResultMapper influxDBResultMapper = new InfluxDBResultMapper();
        return influxDBResultMapper.toPOJO(this.influxQuery("SELECT * FROM measureImu", "imu"),
                ImuMeasurementPoint.class);
    }

    @Override
    public List<ImuMeasurementPoint> getAllPoints(Date startDate, Date endDate, Long sessionId) {
        InfluxDBResultMapper influxDBResultMapper = new InfluxDBResultMapper();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        return influxDBResultMapper.toPOJO(this.influxQuery("SELECT * FROM measureImu WHERE " +
            "time > '" + dateFormat.format(startDate) + "' AND time < '" + dateFormat.format(endDate) +
            "' AND sessionId = " + sessionId, "imu"), ImuMeasurementPoint.class);
    }

    @Override
    public List<ImuMeasurementPoint> getAllPoints(Long sessionId) {
        InfluxDBResultMapper influxDBResultMapper = new InfluxDBResultMapper();
        return influxDBResultMapper.toPOJO(this.influxQuery("SELECT * FROM measureImu WHERE sessionId = " + sessionId, "imu"),
                                           ImuMeasurementPoint.class);
    }

    @Override
    public void savePoint(Point point) {
        InfluxDB influxDb = InfluxDBFactory.connect(influxUrl, influxUser, influxPassword);
        influxDb.setDatabase("imu");
        influxDb.write(point);
    }

    private QueryResult influxQuery(String qp, String database) {
        InfluxDB influxDb = InfluxDBFactory.connect(influxUrl, influxUser, influxPassword);
        Query query = new Query(qp, database);
        return influxDb.query(query);
    }
}
