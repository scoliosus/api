package com.example.demo.Service;

import com.example.demo.Model.imumeasurement.ImuMeasurementPoint;
import com.example.demo.Model.session.SessionResponse;
import java.util.Date;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InfluxService {

    void savePoint(Point point);

    void savePoints(BatchPoints points);

    List<ImuMeasurementPoint> getAllPoints();

    List<ImuMeasurementPoint> getAllPoints(Date startDate, Date endDate, Long sessionId);

    List<ImuMeasurementPoint> getPointsInLastSeconds(Long seconds);

    List<ImuMeasurementPoint> getPointsInLastMilliseconds(Long milliseconds);

    List<SessionResponse> getAllSessions();

    SessionResponse getCurrentSession();

    List<ImuMeasurementPoint> getAllPoints(Long sessionId);
}
