package com.example.demo.Model.session;

import lombok.Data;

@Data
public class SessionResponse {

  private Long sessionId;

  public SessionResponse(Long sessionId) {
    this.sessionId = sessionId;
  }
}
