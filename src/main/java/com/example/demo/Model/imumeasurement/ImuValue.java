package com.example.demo.Model.imumeasurement;

import lombok.Data;

@Data
public class ImuValue {

  private Double x;
  private Double y;
  private Double z;

  public ImuValue(Double x, Double y, Double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}
