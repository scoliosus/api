package com.example.demo.Model.imumeasurement;

import java.time.Instant;
import java.util.concurrent.TimeUnit;
import lombok.Data;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import org.influxdb.dto.Point;

@Measurement(name = "measureImu")
@Data
public class ImuMeasurementPoint {
    @Column(name = "time")
    private Instant time;

    @Column(name = "sessionId")
    private Long sessionId;

    @Column(name = "deviceId")
    private Long deviceId;

    @Column(name = "imu1Lx")
    private Double imu1Lx;

    @Column(name = "imu1Ly")
    private Double imu1Ly;

    @Column(name = "imu1Lz")
    private Double imu1Lz;

    @Column(name = "imu1Rx")
    private Double imu1Rx;

    @Column(name = "imu1Ry")
    private Double imu1Ry;

    @Column(name = "imu1Rz")
    private Double imu1Rz;

    @Column(name = "imu2Lx")
    private Double imu2Lx;

    @Column(name = "imu2Ly")
    private Double imu2Ly;

    @Column(name = "imu2Lz")
    private Double imu2Lz;

    @Column(name = "imu2Rx")
    private Double imu2Rx;

    @Column(name = "imu2Ry")
    private Double imu2Ry;

    @Column(name = "imu2Rz")
    private Double imu2Rz;

    @Column(name = "imu3Lx")
    private Double imu3Lx;

    @Column(name = "imu3Ly")
    private Double imu3Ly;

    @Column(name = "imu3Lz")
    private Double imu3Lz;

    @Column(name = "imu3Rx")
    private Double imu3Rx;

    @Column(name = "imu3Ry")
    private Double imu3Ry;

    @Column(name = "imu3Rz")
    private Double imu3Rz;

    @Column(name = "imu4Lx")
    private Double imu4Lx;

    @Column(name = "imu4Ly")
    private Double imu4Ly;

    @Column(name = "imu4Lz")
    private Double imu4Lz;

    @Column(name = "imu4Rx")
    private Double imu4Rx;

    @Column(name = "imu4Ry")
    private Double imu4Ry;

    @Column(name = "imu4Rz")
    private Double imu4Rz;

    public ImuMeasurementPoint() { }

    public ImuMeasurementPoint(Instant time, Long sessionId, Long deviceId,
                               Double imu1Lx, Double imu1Ly,
                               Double imu1Lz, Double imu1Rx,
                               Double imu1Ry, Double imu1Rz,
                               Double imu2Lx, Double imu2Ly,
                               Double imu2Lz, Double imu2Rx,
                               Double imu2Ry, Double imu2Rz,
                               Double imu3Lx, Double imu3Ly,
                               Double imu3Lz, Double imu3Rx,
                               Double imu3Ry, Double imu3Rz,
                               Double imu4Lx, Double imu4Ly,
                               Double imu4Lz, Double imu4Rx,
                               Double imu4Ry, Double imu4Rz) {
        this.time = time;
        this.sessionId = sessionId;
        this.deviceId = deviceId;
        this.imu1Lx = imu1Lx;
        this.imu1Ly = imu1Ly;
        this.imu1Lz = imu1Lz;
        this.imu1Rx = imu1Rx;
        this.imu1Ry = imu1Ry;
        this.imu1Rz = imu1Rz;
        this.imu2Lx = imu2Lx;
        this.imu2Ly = imu2Ly;
        this.imu2Lz = imu2Lz;
        this.imu2Rx = imu2Rx;
        this.imu2Ry = imu2Ry;
        this.imu2Rz = imu2Rz;
        this.imu3Lx = imu3Lx;
        this.imu3Ly = imu3Ly;
        this.imu3Lz = imu3Lz;
        this.imu3Rx = imu3Rx;
        this.imu3Ry = imu3Ry;
        this.imu3Rz = imu3Rz;
        this.imu4Lx = imu4Lx;
        this.imu4Ly = imu4Ly;
        this.imu4Lz = imu4Lz;
        this.imu4Rx = imu4Rx;
        this.imu4Ry = imu4Ry;
        this.imu4Rz = imu4Rz;
    }

    public Point toPoint() {
        return Point.measurement("measureImu")
            .time(this.getTime().toEpochMilli(), TimeUnit.MILLISECONDS)
            .addField("sessionId", this.getSessionId())
            .addField("deviceId", this.getDeviceId())
            .addField("imu1Lx", this.getImu1Lx())
            .addField("imu1Ly", this.getImu1Ly())
            .addField("imu1Lz", this.getImu1Lz())
            .addField("imu1Rx", this.getImu1Rx())
            .addField("imu1Ry", this.getImu1Ry())
            .addField("imu1Rz", this.getImu1Rz())
            .addField("imu2Lx", this.getImu2Lx())
            .addField("imu2Ly", this.getImu2Ly())
            .addField("imu2Lz", this.getImu2Lz())
            .addField("imu2Rx", this.getImu2Rx())
            .addField("imu2Ry", this.getImu2Ry())
            .addField("imu2Rz", this.getImu2Rz())
            .addField("imu3Lx", this.getImu3Lx())
            .addField("imu3Ly", this.getImu3Ly())
            .addField("imu3Lz", this.getImu3Lz())
            .addField("imu3Rx", this.getImu3Rx())
            .addField("imu3Ry", this.getImu3Ry())
            .addField("imu3Rz", this.getImu3Rz())
            .addField("imu4Lx", this.getImu4Lx())
            .addField("imu4Ly", this.getImu4Ly())
            .addField("imu4Lz", this.getImu4Lz())
            .addField("imu4Rx", this.getImu4Rx())
            .addField("imu4Ry", this.getImu4Ry())
            .addField("imu4Rz", this.getImu4Rz())
            .build();
    }
}
