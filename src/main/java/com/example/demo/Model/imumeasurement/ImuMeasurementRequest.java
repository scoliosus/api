package com.example.demo.Model.imumeasurement;

import java.util.Date;
import lombok.Data;

@Data
public class ImuMeasurementRequest {

    private Long sessionId;
    private Long deviceId;
    private Date timestamp;
    private ImuValue imu1L;
    private ImuValue imu1R;
    private ImuValue imu2L;
    private ImuValue imu2R;
    private ImuValue imu3L;
    private ImuValue imu3R;
    private ImuValue imu4L;
    private ImuValue imu4R;

    public ImuMeasurementRequest(Long sessionId, Long deviceId, Date timestamp, ImuValue imu1L,
                                 ImuValue imu1R, ImuValue imu2L, ImuValue imu2R, ImuValue imu3L,
                                 ImuValue imu3R, ImuValue imu4L, ImuValue imu4R) {
        this.sessionId = sessionId;
        this.deviceId = deviceId;
        this.timestamp = timestamp;
        this.imu1L = imu1L;
        this.imu1R = imu1R;
        this.imu2L = imu2L;
        this.imu2R = imu2R;
        this.imu3L = imu3L;
        this.imu3R = imu3R;
        this.imu4L = imu4L;
        this.imu4R = imu4R;
    }

    public ImuMeasurementPoint toModel() {
        return new ImuMeasurementPoint(this.getTimestamp().toInstant(),
            this.getSessionId(),
            this.getDeviceId(),
            this.getImu1L().getX(),
            this.getImu1L().getY(),
            this.getImu1L().getZ(),
            this.getImu1R().getX(),
            this.getImu1R().getY(),
            this.getImu1R().getZ(),
            this.getImu2L().getX(),
            this.getImu2L().getY(),
            this.getImu2L().getZ(),
            this.getImu2R().getX(),
            this.getImu2R().getY(),
            this.getImu2R().getZ(),
            this.getImu3L().getX(),
            this.getImu3L().getY(),
            this.getImu3L().getZ(),
            this.getImu3R().getX(),
            this.getImu3R().getY(),
            this.getImu3R().getZ(),
            this.getImu4L().getX(),
            this.getImu4L().getY(),
            this.getImu4L().getZ(),
            this.getImu4R().getX(),
            this.getImu4R().getY(),
            this.getImu4R().getZ());
    }
}
