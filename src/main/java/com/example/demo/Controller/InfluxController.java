package com.example.demo.Controller;

import com.example.demo.Model.imumeasurement.ImuMeasurementPoint;
import com.example.demo.Model.imumeasurement.ImuMeasurementRequest;
import com.example.demo.Model.imumeasurement.ImuValue;
import com.example.demo.Model.session.SessionResponse;
import com.example.demo.Service.InfluxService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.influxdb.dto.BatchPoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/influx")
public class InfluxController {

    private InfluxService influxService;

    @Autowired
    public InfluxController(InfluxService influxService) {
        this.influxService = influxService;
    }

    @PostMapping("/insert")
    public ResponseEntity insertPoint(@RequestBody ImuMeasurementRequest request) {
        this.influxService.savePoint(request.toModel().toPoint());
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PostMapping("/insert-multiple")
    public ResponseEntity insertPoint(@RequestBody List<ImuMeasurementRequest> request) {
        BatchPoints batchPoints =   BatchPoints.database("imu")
                .retentionPolicy("autogen")
                .build();

        request.forEach(item -> batchPoints.point(item.toModel().toPoint()));
        this.influxService.savePoints(batchPoints);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<ImuMeasurementPoint>> getPoints() {
        return ResponseEntity.ok(this.influxService.getAllPoints());
    }

    @GetMapping("/seconds")
    public ResponseEntity<List<ImuMeasurementPoint>> getPoints(@RequestParam Long seconds) {
        return ResponseEntity.ok(this.influxService.getPointsInLastSeconds(seconds));
    }

    @GetMapping("/milliseconds")
    public ResponseEntity<List<ImuMeasurementPoint>> getMilliPoints(@RequestParam Long milliseconds) {
        return ResponseEntity.ok(this.influxService.getPointsInLastMilliseconds(milliseconds));
    }

    @GetMapping("/sessions")
    public ResponseEntity<List<SessionResponse>> getAllSessions() {
        return ResponseEntity.ok(this.influxService.getAllSessions());
    }

    @GetMapping("/sessions/{sessionId}")
    public ResponseEntity<List<ImuMeasurementPoint>> getAllSessions(@PathVariable Long sessionId,
                                                                    @RequestParam(required = false) String startDate,
                                                                    @RequestParam(required = false) String endDate)
        throws ParseException {
        if (startDate != null && endDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return ResponseEntity.ok(this.influxService.getAllPoints(dateFormat.parse(startDate),
                                                                     dateFormat.parse(endDate), sessionId));
        }
        return ResponseEntity.ok(this.influxService.getAllPoints(sessionId));
    }

    @GetMapping("/sessions/current")
    public ResponseEntity<SessionResponse> getCurrentSession() {
        return ResponseEntity.ok(this.influxService.getCurrentSession());
    }

    @GetMapping("/insert-dummy")
    public void insertDummy() {
        Random r = new Random();
        Long randomSession = (long) r.nextInt(4) + 1;
        ImuMeasurementRequest measurementRequest = new ImuMeasurementRequest(
            randomSession,
            (long) 1,
            new Date(),
            this.getRandomImuValue(),
            this.getRandomImuValue(),
            this.getRandomImuValue(),
            this.getRandomImuValue(),
            this.getRandomImuValue(),
            this.getRandomImuValue(),
            this.getRandomImuValue(),
            this.getRandomImuValue());
        this.influxService.savePoint(measurementRequest.toModel().toPoint());
    }

    private ImuValue getRandomImuValue() {
        Random r = new Random();
        return new ImuValue(
            180 * r.nextDouble() - 90,
            180 * r.nextDouble() - 90,
            180 * r.nextDouble() - 90
        );
    }
}
